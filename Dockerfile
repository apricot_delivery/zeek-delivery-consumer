FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
COPY delivery-consumer.go ./
COPY t-dispatcher-335004-282425de0503.json ./
RUN go mod download
ENV GOOGLE_APPLICATION_CREDENTIALS "/app/t-dispatcher-335004-282425de0503.json"

ENTRYPOINT ["go","run","/app/delivery-consumer.go"]