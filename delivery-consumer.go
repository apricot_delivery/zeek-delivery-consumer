package main
//TODO: Total should accept decimals
import (
        "context"
        "fmt"
        "sync"
        "bytes"
        "strconv"
        b64 "encoding/base64"
        "encoding/json"
        "net/http"
        "cloud.google.com/go/pubsub"
        "github.com/machinebox/graphql"
        "github.com/ttacon/libphonenumber"
        "time"
        "strings"
        "errors"
)

type AddressExtraInfo struct {
    AdditionalInformation string `json:"additionalInformation"`
    Lat float64 `json:"latitude"`
    Long float64 `json:"longitude"`

}
type OrderResponse struct {
    Order Order `json:"Order"`
}
type Order struct {
    Created time.Time `json:"created"`
    Id string `json:"id"`
    Number string `json:"number"`
    Status string `json:"status"`
    User User `json:"user"`
    Address ShippingAddress `json:"shippingAddress"`
    TotalAmount Total `json:"total"`
    TotalCaptured Money `json:"totalCaptured"`
    MetaData []MapStruct `json:"metaData"`
}
type Total struct {
    Gross Money `json:"gross"`
}
type Money struct {
    Amount float32 `json:"amount"`
}
type ShippingAddress struct {
    Id string `json:"id"`
    CompanyName string `json:"companyName"`
    FirstName string `json:"firstName"`
    LastName string `json:"lastName"`
    StreetAddress1 string `json:"streetAddress1"`
    StreetAddress2 string `json:"streetAddress2"`
    City string `json:"city"`
    PostalCode string `json:"postalCode"`
    Phone string `json:"phone"`
}

type User struct {
    Id string `json:"id"`
    Metadata []MapStruct 
}
type MapStruct struct {
    Key string `json:"key"`
    Value string `json:"value"`
}
type ZeekRequest struct {
    Auth Auth `json:"auth"`
    Data Data `json:"data"`
}
type ZeekResponse struct {
    Error uint32 `json:"error"`
    ErrorMessage string `json:"err_msg"`
    Data ZeekDataResponse `json:"data"`
}
type ZeekDataResponse struct {
    OrderId string `json:"order_id"`
}
type Auth struct {
  Appid int `json:"appid"`
  Timestamp int64 `json:"timestamp"`
  Signature string `json:"signature"`
}

type Receive struct {
  Name string `json:"user_name"`
  Phone string `json:"user_phone"`
  CountryCode string `json:"user_phone_country_code"`
  Location string `json:"user_location"`
  Address string `json:"user_address"`
}
type Data struct {
  ClientMerchantId string `json:"client_merchant_id"`
  ClientOrderId string `json:"client_order_id"`
  MerchantOrderId string `json:"merchant_order_id"`
  OrderTime int64 `json:"order_time"`
  Remark string `json:"remark"`
  MerchantRemark string `json:"merchant_remark"`
  CodType uint8 `json:"cod_type"`
  Receive Receive `json:"receive"`
  Meta map[string]string `json:"meta"`
  OrderDetail map[string]uint32  `json:"order_detail"`
}

type UserData struct {
    UserData UserAddress `json:"user"`
}

type Address struct {
    Id string `json:"id"`
    Metadata []MapStruct `json:"metadata"`
}

type UserAddress struct {
    Addresses []Address `json:"addresses"`
}



func main() {
    projectID := "t-dispatcher-335004"
    subID := "orders-zeek"

    //if(subID == ""){
        fmt.Println(pullMsgs(projectID,subID))
    //}
    //fmt.Println(processOrder("T3JkZXI6NDg3"))

}

func processOrder (orderId string) error {
    order, err := getOrder(orderId);
    
    if (err != nil) {
        return err;
    }

    if shouldBeSent(order.Order) {


        if err != nil {
            fmt.Println("OrderId: " + orderId + " error: " + err.Error())
        }

        zeekRef, err := postZeekOrder(order.Order);


        fmt.Println("zeekRef: " + zeekRef)
        if err != nil {
            return err;
        }
        updateZeekReference(order.Order.Id,zeekRef)

        if err != nil {
            return err;
        }
    } else {
        fmt.Println("Skipping order " + order.Order.Id)
    }
    return nil
}
func shouldBeSent(order Order) bool {
    return getValueOf(order.MetaData,"zeekReference") == "" &&
    order.Status!="CANCELED" && order.Status!="DRAFT" && 
    !strings.HasPrefix(order.Address.Phone , "+54") && len(order.Id)>1
}
func getValueOf(metadata []MapStruct, key string) string{
    for _, element := range metadata {
        if element.Key == key {
            return element.Value 
        }
    }
    return ""
}
func updateZeekReference(orderId string, reference string) error {
    client := graphql.NewClient("https://v1-api.apricot.delivery/graphql/")
    req := graphql.NewRequest(`
        mutation ($orderId: ID!, $value: String!) {
            updateMetadata ( id: $orderId,
            input:[{
              key: "zeekReference"
              value: $value
            }]) 
            {
                item{
                    metadata {
                        key,
                        value
                    }
                }
            }
        }
    `)
    req.Var("orderId", orderId)
    req.Var("value", reference)

    req.Header.Set("Cache-Control", "no-cache")
    req.Header.Set("Authorization","Bearer QunbRVm3UEL7HQ4n9sDzjx4MKnfzKa")

    // define a Context for the request
    ctx := context.Background()

    // run it and capture the response
    var respData OrderResponse
    if err := client.Run(ctx, req, &respData); err != nil {
        return err;
    }
    return nil;
}

func getOrder(orderId string) (OrderResponse, error) {
    client := graphql.NewClient("https://v1-api.apricot.delivery/graphql/")

    // make a request
    req := graphql.NewRequest(`
        query($orderId: ID!) {
            order(id:$orderId) {
                id,
                status,
                created,
                number,
                user {
                    id
                     metadata {
                        key
                        value
                    }
                }
                total {
                    gross {
                        amount
                    }
                }
                totalCaptured {
                    amount
                }
                metadata {
                    key,
                    value
                }
                shippingAddress {
                    id,
                    firstName,
                    lastName,
                    companyName,
                    streetAddress1,
                    streetAddress2,
                    city,
                    postalCode,
                    phone
                }
                fulfillments {
                    status
                }
            }
        }
    `)

    req.Var("orderId", orderId)
    // set header fields
    req.Header.Set("Cache-Control", "no-cache")
    req.Header.Set("Authorization","Bearer QunbRVm3UEL7HQ4n9sDzjx4MKnfzKa")

    // define a Context for the request
    ctx := context.Background()

    // run it and capture the response
    var respData OrderResponse
    if err := client.Run(ctx, req, &respData); err != nil {
        return respData, err
    }
    return respData, nil;
}


func pullMsgs(projectID, subID string) error {
    ctx := context.Background()
    client, err := pubsub.NewClient(ctx, projectID)
    if err != nil {
        fmt.Println("pubsub.NewClient: %v", err)
        return err
    }
    defer client.Close()

    var mu sync.Mutex
    sub := client.Subscription(subID)

    err = sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {
        var jsonMap []map[string]interface{}
        mu.Lock()
        defer mu.Unlock()
        err := json.Unmarshal(msg.Data, &jsonMap)

        if(err == nil) {
            orderId := jsonMap[0]["id"].(string)
            err := processOrder(orderId)

            if(err == nil) {
                msg.Ack()
            } else {
                fmt.Println("Error processingOrder:", orderId, err)
            }
        } else {
            fmt.Println("Error unmarshalling:", err)
        }
        
    })
    return err
}


func postZeekOrder(order Order)  (string, error) { 

    phone := order.Address.Phone 

    if (phone == "") {
        phone = "+66629804058"
    }

    company := order.Address.CompanyName

    if (company == "") {
        company = "ewogICJhZGRpdGlvbmFsSW5mb3JtYXRpb24iIDogIiIsCiAgImxvbmdpdHVkZSIgOiAxMDAuNTc0Nzk3Njg3MzE3OTEsCiAgImxhdGl0dWRlIiA6IDEzLjczMzExNDAxODI1ODUzMQp9"
    }

    num, err := libphonenumber.Parse(phone, "")

    if err != nil {
        return "", err
    }

    countryCode := num.GetCountryCode()
    nationalNumber := num.GetNationalNumber()

    var digits = "0"
    if(countryCode == 66){
        digits = "10"
    }
    if(countryCode == 65){
        digits = "8"
    }


    totalOutstanding := uint32 (order.TotalAmount.Gross.Amount*100);
    
    auth := Auth {
        Appid: 1092,
        Timestamp: time.Now().Unix(),
        Signature: "12f6f714b818aca83c3cea1efa15a50edc92339c",
    } 

    base64Bytes, err := b64.StdEncoding.DecodeString(company) // Converting data
            
    if err != nil {
        return "",err
    }


    var addressExtraInfo AddressExtraInfo
    err = json.Unmarshal(base64Bytes, &addressExtraInfo)
    
    //fmt.Println(string(base64Bytes))
    
    if err != nil {
        return "", err
    }
    location := strconv.FormatFloat(addressExtraInfo.Lat,'f', -1, 64) + "," + strconv.FormatFloat(addressExtraInfo.Long,'f', -1, 64) 
    additionalInformation := addressExtraInfo.AdditionalInformation

    receive := Receive {
        Name: "---",
        Phone: fmt.Sprintf("%0"+digits+"d",nationalNumber),
        CountryCode: fmt.Sprint(countryCode),
        Location: location ,
        Address: order.Address.StreetAddress1 + " " + order.Address.StreetAddress2 + "," + order.Address.City + " " + order.Address.PostalCode, 
    }
    data := Data {
        ClientMerchantId: "V2FyZWhvdXNlOmUxZjY0YjM3LTdiNWEtNDBhNi1iYWYyLWNmYTNmNDI1NWE3NA",
        ClientOrderId: order.Id,
        MerchantOrderId: fmt.Sprintf("%04s",order.Number),
        OrderTime: order.Created.Unix(),
        Remark: additionalInformation,
        CodType: 1,
        Receive: receive,
        Meta: map[string]string{
            "region":"BKK",
            "lang":"en",
        },
        OrderDetail: map[string]uint32{
            "total_price": totalOutstanding,
        },
    }
    
    zeekRequest := &ZeekRequest {
        Auth: auth,
        Data:data,
    } 

    bytesRepresentation, err := json.Marshal(zeekRequest)

    fmt.Println(string(bytesRepresentation))
    if err != nil {
        return "", err
    }
    

    req, err := http.NewRequest("POST", "https://ap2-open-api.zeek.one/v1.3/order/takeaway/create", bytes.NewBuffer(bytesRepresentation))

    if err != nil {
        return "", err
    }

    req.Header.Set("Content-type", "application/json")
    req.Header.Set("Accept", "*/*")
    req.Header.Set("User-Agent", "PostmanRuntime/7.29.0")
    req.Header.Set("Cache-Control", "no-cache")
    client := &http.Client{}
    resp, err := client.Do(req)

    defer resp.Body.Close()

    if err != nil {
        return "", err
    }

    var result ZeekResponse

    json.NewDecoder(resp.Body).Decode(&result)

    if result.Error > 0 {
        return "", errors.New("Error!:" + order.Id + " - " + string(result.Error) + "-" +result.ErrorMessage + string(bytesRepresentation))
    }
    return result.Data.OrderId, nil
}
